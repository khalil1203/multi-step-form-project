
const nextbtn = document.querySelector('.next-btn');
const goBack = document.querySelector('.back-btn');
nextbtn.addEventListener('click', formCheck);
goBack.addEventListener('click', countDecrease);
const step1 = document.querySelector('.step1');
const circle1 = document.querySelector('.circle1');
const step2 = document.querySelector('.step2');
const circle2 = document.querySelector('.circle2');
const step3 = document.querySelector('.step3');
const circle3 = document.querySelector('.circle3');
const step4 = document.querySelector('.step4');
const circle4 = document.querySelector('.circle4');
const togglebtn = document.querySelector('.custom-control-input');
togglebtn.addEventListener('click', page2Func);
const check1 = document.querySelector('.check1');
check1.addEventListener('click', addons);
const check2 = document.querySelector('.check2');
check2.addEventListener('click', addons);
const check3 = document.querySelector('.check3');
check3.addEventListener('click', addons);
const finalPagename = document.querySelector('.finalpage-name');

const finalPageprice = document.querySelector('.finalpage-price');

let checkboxArr = [false, false, false, 0, 0, 0];
let currentStep = 1;
let toggleStatus = 0;
let planSlected = 0;
let obj = [];
document.querySelector(".red").style.display = "none";

function formCheck() {
    let valid1 = false;
    let valid2 = false;
    let valid3 = false;
    const mob = document.querySelector('#phone').value;
    const name = document.querySelector('#name').value;
    const email = document.querySelector('#email').value;
    if (name.length < 1) {
        document.querySelector("#name").style.border = "1px solid red";
    }
    else {
        document.querySelector("#name").style.border = "1px solid black";
        valid1 = true
    }

    if (email.length < 1) {
        document.querySelector("#email").style.border = "1px solid red";
    }
    else {
        document.querySelector("#email").style.border = "1px solid black";
        valid2 = true;
    }

    if (mob.length < 10) {
        document.querySelector(".red").style.display = "block";
    }
    else {
        valid3 = true;
    }
    if (valid1 && valid2 && valid3) {
        nextbtn.removeEventListener('click', formCheck);
        nextbtn.addEventListener('click', count);
        count();
    }

    console.log(mob);
}



const arcade = document.querySelector('.arcade');
const pro = document.querySelector('.pro');
const advanced = document.querySelector('.advanced');
arcade.addEventListener('click', selected);
pro.addEventListener('click', selected);
advanced.addEventListener('click', selected);


if (toggleStatus === 0) {
    document.querySelector('.change1').innerHTML = "<div>$9/mo</div>";
    document.querySelector('.change2').innerHTML = "<div>$12/mo</div>";
    document.querySelector('.change3').innerHTML = "<div>$15/mo</div>";
    document.querySelector('.monthly').style.opacity = "100%";
    document.querySelector('.yearly').style.opacity = "50%";
}

const dataContainer = document.querySelector('.data-container').children;
console.log(dataContainer);


function count() {
    if (currentStep < 6) {
        currentStep++;
    }
    updatePage();
}

function countDecrease() {
    if (currentStep > 2) {
        currentStep--;
    }
    updatePage();
}



if (currentStep === 1) {
    step2.style.opacity = '40%';
    step3.style.opacity = '40%';
    step4.style.opacity = '40%';
    step1.style.opacity = '100%';
    circle1.style.backgroundColor = "#BEE1FF";
    circle1.style.color = "#164A8A";
    enablePage();
    disablePage();
    goBack.style.display = "none";

}



function disablePage() {
    for (let index = 0; index < 5; index++) {
        if (index != currentStep - 1) {
            dataContainer[index].style.display = "none";
        }
    }
}
function enablePage() {
    dataContainer[currentStep - 1].style.display = "block";

}

function updatePage() {

    if (currentStep === 1) {
        step2.style.opacity = '40%';
        step3.style.opacity = '40%';
        step4.style.opacity = '40%';
        step1.style.opacity = '100%';
        circle1.style.backgroundColor = "#BEE1FF";
        circle1.style.color = "#164A8A";
        enablePage();
        disablePage();

    }
    if (currentStep === 2) {
        step2.style.opacity = '100%';
        step1.style.opacity = '40%';
        step3.style.opacity = '40%';
        step4.style.opacity = '40%';
        circle2.style.backgroundColor = "#BEE1FF";
        circle2.style.color = "#164A8A";
        enablePage();
        disablePage();
        dataContainer[currentStep - 1].style.display = "flex";
        goBack.style.display = "block";
        goBack.style.display = "flex";
        document.querySelector('.buttons-div').style.justifyContent = "space-around";


        if (planSlected === 0) {
            console.log("you are rremoving event listener")
            nextbtn.removeEventListener('click', count);
        }



    }
    if (currentStep === 3) {
        step2.style.opacity = '40%';
        step1.style.opacity = '40%';
        step4.style.opacity = '40%';
        step3.style.opacity = '100%';
        circle3.style.backgroundColor = "#BEE1FF";
        circle3.style.color = "#164A8A";
        enablePage();
        disablePage();
        dataContainer[currentStep - 1].style.display = "flex";


    }
    if (currentStep === 4) {
        step3.style.opacity = '40%';
        step2.style.opacity = '40%';
        step1.style.opacity = '40%';
        step4.style.opacity = '100%';
        circle4.style.backgroundColor = "#BEE1FF";
        circle4.style.color = "#164A8A";
        enablePage();
        disablePage();

        nextbtn.innerHTML = `<button class="btns confirm">Confirm</button>`;

        // dataContainer[currentStep - 1].style.display = "flex";
        finalPage();


    }
    if (currentStep === 5) {
        step3.style.opacity = '40%';
        step2.style.opacity = '40%';
        step1.style.opacity = '40%';

        step4.style.opacity = '100%';
        circle4.style.backgroundColor = "#BEE1FF";
        circle4.style.color = "#164A8A";
        enablePage();
        disablePage();
        document.querySelector('.buttons-div').style.display = "none";
        dataContainer[currentStep - 1].style.display = "flex";


    }

}


function page2Func() {
    console.log("you are in pag2 function")
    if (toggleStatus === 0) {
        toggleStatus = 1;
        document.querySelector('.change1').innerHTML = "<div>$90/yr<br>2 months free</div>";
        document.querySelector('.change2').innerHTML = "<div>$120/yr<br>2 months free</div>";
        document.querySelector('.change3').innerHTML = "<div>$150/yr<br>2 months free</div>";
        document.querySelector('.monthly').style.opacity = "50%";
        document.querySelector('.yearly').style.opacity = "100%";
    }
    else {
        toggleStatus = 0;
        document.querySelector('.change1').innerHTML = "<div>$9/mo</div>";
        document.querySelector('.change2').innerHTML = "<div>$12/mo</div>";
        document.querySelector('.change3').innerHTML = "<div>$15/mo</div>";
        document.querySelector('.monthly').style.opacity = "100%";
        document.querySelector('.yearly').style.opacity = "50%";
    }
}



function selected() {
    planSlected = 1;
    console.log(this.className.split(" ")[0]);
    nextbtn.addEventListener('click', count);
    obj[0] = {
        name: this.className.split(" ")[0]
    }
    this.style.border = "1px solid violet";
    this.style.backgroundColor = "#f8f9fe";
    let arr = document.querySelector('.plans').children
    for (let index = 0; index < 3; index++) {
        if (arr[index] != this) {
            arr[index].style.border = "1px solid black";
            arr[index].style.backgroundColor = "#FFFFFF";
        }
    }
}




function addons() {
    console.log(this.className.split(" ")[0]);
    if (this.className.split(" ")[0] == "check1") {
        if (checkboxArr[0]) {
            checkboxArr[0] = !checkboxArr[0];
            this.parentNode.style.border = "1px solid blue";
            this.parentNode.style.backgroundColor = "#FFFFFF";
        }
        else {
            checkboxArr[0] = !checkboxArr[0];
            this.parentNode.style.border = "1px solid violet";
            this.parentNode.style.backgroundColor = "#f8f9fe";
        }
    }

    if (this.className.split(" ")[0] == "check2") {
        if (checkboxArr[1]) {
            checkboxArr[1] = !checkboxArr[1];
            this.parentNode.style.border = "1px solid blue";
            this.parentNode.style.backgroundColor = "#FFFFFF";
        }
        else {
            checkboxArr[1] = !checkboxArr[1];
            this.parentNode.style.border = "1px solid violet";
            this.parentNode.style.backgroundColor = "#f8f9fe";
        }
    }
    if (this.className.split(" ")[0] == "check3") {
        if (checkboxArr[2]) {
            checkboxArr[2] = !checkboxArr[2];
            this.parentNode.style.border = "1px solid blue";
            this.parentNode.style.backgroundColor = "#FFFFFF";
        }
        else {
            checkboxArr[2] = !checkboxArr[2];
            this.parentNode.style.border = "1px solid violet";
            this.parentNode.style.backgroundColor = "#f8f9fe";
        }
    }

}



function finalPage() {
    let finalPagerow2 = document.querySelector('.finalpage-row2');
    finalPage.innerHTML = "";
    finalPagerow2.innerHTML = "";
    let datadiv = document.createElement('div');
    let planName = obj[0].name;
    let change;
    let total = 0;
    if (planName == "arcade" && toggleStatus == 0) {
        total = 9;
        finalPagename.innerHTML = `Arcade (Monthly)<br><a href="#" onclick="changeFunc()">Change</a>`;
        finalPageprice.innerHTML = "$9/mo";
    }
    if (planName == "arcade" && toggleStatus == 1) {
        planPrice = "$90/yr";
        total = 90;
        finalPagename.innerHTML = `Arcade (Yearly)<br><a href="#" onclick="changeFunc()">Change</a>`;
        finalPageprice.innerHTML = "$90/yr";
    }


    if (planName == "pro" && toggleStatus == 0) {
        total = 15;
        finalPagename.innerHTML = `Pro (Monthly)<br><a href="#" onclick="changeFunc()">Change</a>`;
        finalPageprice.innerHTML = "$15/mo";
    }
    if (planName == "pro" && toggleStatus == 1) {
        planPrice = "$150/yr";
        total = 150;
        finalPagename.innerHTML = `Pro (Yearly)<br><a href="#" onclick="changeFunc()">Change</a>`;
        finalPageprice.innerHTML = "$150/yr";
    }


    if (planName == "advanced" && toggleStatus == 0) {
        total = 12;
        finalPagename.innerHTML = `Advanced (Monthly)<br><a href="#" onclick="changeFunc()">Change</a>`;
        finalPageprice.innerHTML = "$12/mo";
    }
    if (planName == "advanced" && toggleStatus == 1) {
        planPrice = "$120/yr";
        total = 120;
        finalPagename.innerHTML = `Advanced (Yearly)<br><a href="#" onclick="changeFunc()">Change</a>`;
        finalPageprice.innerHTML = "$120/yr";
    }
    if (checkboxArr[0]) {
        finalPagerow2.innerHTML += `<div class="row2-div"><div class="opacity-30">Online service</div><div class="padding-right-0">+$1/mo</div><div>`;
        total += 1;
    }
    if (checkboxArr[1]) {
        finalPagerow2.innerHTML += `<div class="row2-div"><div class="opacity-30">Larger storage</div><div class="padding-right-0">+$2/mo</div><div>`;
        total += 2;
    }
    if (checkboxArr[2]) {
        finalPagerow2.innerHTML += `<div class="row2-div"><div class="opacity-30">Customizable Profile</div><div class="padding-right-0">+$2/mo</div><div>`;
        total += 2;
    }
    document.querySelector('.total-price').innerHTML = `<div>Total</div><div>$${total}</div>`;
}


function changeFunc() {
    currentStep = 2;
    updatePage();
}

